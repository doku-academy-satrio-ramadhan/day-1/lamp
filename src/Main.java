public class Main {
    public static void main(String[] args) {
        int n = 4;
        boolean isOn = false;

        for(int i = 1; i <= n; i++){
            if(n % i == 0){
                isOn = !isOn;
            }
        }

        if(isOn){
            System.out.println("lampu menyala");
        }else{
            System.out.println("lampu mati");
        }
    }
}